package com.gitee.spirit.editor.controller;

import com.gitee.spirit.editor.entity.MethodInfo;
import com.gitee.spirit.editor.entity.ResObject;
import com.gitee.spirit.editor.service.EditorService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/editor")
public class EditorController {

    @Autowired
    public EditorService editorService;

    @PostMapping("/parseMethodInfos")
    public ResObject<List<MethodInfo>> parseMethodInfos(@RequestBody ParseMethodInfosDto dto) {
        return editorService.parseMethodInfos(dto);
    }

    @Data
    public static class ParseMethodInfosDto {
        private String filePath;
        private String content;
        private Integer lineNumber;
    }

}
