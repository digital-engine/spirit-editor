package com.gitee.spirit.editor.core;

import com.gitee.spirit.core3.compile.api.CompileListenerAdapter;
import com.gitee.spirit.core3.compile.entity.CompilationUnit;
import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseContext;
import com.gitee.spirit.core3.compile.entity.SCType;
import com.gitee.spirit.core3.element.entity.Element;
import com.gitee.spirit.core3.element.entity.Statement;
import com.gitee.spirit.core3.tree.entity.ElementEvent;
import com.gitee.spirit.core3.tree.entity.clazz.ClassTree;
import com.gitee.spirit.core3.tree.entity.clazz.CompilationTree;
import com.gitee.spirit.core3.tree.entity.clazz.MethodTree;
import com.gitee.spirit.core3.visitor.api.ClassProxy;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.api.CompilationVisitor;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.editor.entity.FileLine;
import com.gitee.spirit.editor.entity.MethodInfo;
import com.gitee.spirit.editor.entity.ResObject;
import com.google.common.base.Joiner;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@Order(-120)
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomCompileListener extends CompileListenerAdapter {

    private final URL fileURL;
    private final String content;
    private CustomElementListener customElementListener;
    private StatementParser statementParser;
    private CompilationLinker compilationLinker;
    private CompilationVisitor compilationVisitor;
    private ResObject<List<MethodInfo>> resObject;

    @Override
    public void fileObjectParsed(ParseContext parseContext, FileObject fileObject) {
        if (fileURL.sameFile(fileObject.getFileURL())) {
            fileObject.setContent(content);
        }
    }

    @Override
    public void semanticsParsed(ParseContext parseContext, List<CompilationUnit> compilationUnits) {
        // 获取编译后结果
        FileLine fileLine = customElementListener.getFileLine();
        String incompleteName = fileLine.getIncompleteName();

        ElementEvent elementEvent = customElementListener.getElementEvent();
        Element element = elementEvent.getElement();
        Statement statement = element.subStmt(1, element.size());
        SCType statementType = statementParser.parseStmtType(statement);

        // 原始类型和数组类型没有方法
        if (statementType.isPrimitive() || statementType.isArray()) {
            MethodInfo methodInfo = new MethodInfo("No method found! type: " + statementType.getClassName(), "");
            resObject = ResObject.successData(Collections.singletonList(methodInfo));
            return;
        }

        // 通过链接器获取类型
        Object classObject = compilationLinker.getClassObject(statementType);
        if (classObject == null) {
            MethodInfo methodInfo = new MethodInfo("Type not found! type: " + statementType.getClassName(), "");
            resObject = ResObject.successData(Collections.singletonList(methodInfo));
        } else {
            parseClassObject(parseContext, statementType, classObject, incompleteName);
        }
    }

    private void parseClassObject(ParseContext parseContext, SCType statementType, Object classObject, String incompleteName) {
        List<MethodInfo> methodInfos = new ArrayList<>();
        if (classObject instanceof ClassProxy) {
            ClassProxy classProxy = (ClassProxy) classObject;
            compilationVisitor.visitCompilationTree(parseContext, classProxy.getCompilationTree());
            parseClassProxy(classProxy, incompleteName, methodInfos);

        } else if (classObject instanceof Class) {
            parseNativeClass((Class<?>) classObject, incompleteName, methodInfos);
        }
        if (methodInfos.isEmpty()) {
            MethodInfo methodInfo = new MethodInfo("No method found!type: " + statementType.getClassName(), "");
            resObject = ResObject.successData(Collections.singletonList(methodInfo));
        } else {
            resObject = ResObject.successData(methodInfos);
        }
    }

    private void parseClassProxy(ClassProxy classProxy, String incompleteName, List<MethodInfo> methodInfos) {
        CompilationTree compilationTree = classProxy.getCompilationTree();
        for (String methodName : compilationTree.getMethodTrees().keySet()) {
            if (methodName.startsWith(incompleteName)) {
                for (MethodTree methodTree : compilationTree.getMethodTrees().get(methodName)) {
                    methodInfos.add(createMethodInfo(methodTree, incompleteName));
                }
            }
        }
        ClassTree classTree = compilationTree.getClassTree();
        for (String methodName : classTree.getMethodTrees().keySet()) {
            if (methodName.startsWith(incompleteName)) {
                for (MethodTree methodTree : classTree.getMethodTrees().get(methodName)) {
                    if (!methodTree.isConstructor()) {
                        methodInfos.add(createMethodInfo(methodTree, incompleteName));
                    }
                }
            }
        }
    }

    private void parseNativeClass(Class<?> clazz, String incompleteName, List<MethodInfo> methodInfos) {
        for (Method method : clazz.getMethods()) {
            if (method.getName().startsWith(incompleteName)) {
                methodInfos.add(createMethodInfo(method, incompleteName));
            }
        }
    }

    private MethodInfo createMethodInfo(MethodTree methodTree, String incompleteName) {
        String tipText = methodTree.getReturnType().getSimpleName() + " " + methodTree.getMethodToken();
        List<String> parameterNames = new ArrayList<>();
        methodTree.getParameterTrees().forEach(param -> parameterNames.add(param.getParameterName()));
        String actualText = methodTree.getMethodName() + "(" + Joiner.on(", ").join(parameterNames) + ")";
        if (StringUtils.isNotBlank(incompleteName) && actualText.startsWith(incompleteName)) {
            actualText = actualText.replaceFirst(incompleteName, "");
        }
        return new MethodInfo(tipText, actualText);
    }

    private MethodInfo createMethodInfo(Method method, String incompleteName) {
        String tipText = method.toString();
        String actualText = method.toString();
        if (StringUtils.isNotBlank(incompleteName)) {
            actualText = actualText.substring(actualText.indexOf(incompleteName));
            actualText = actualText.replaceFirst(incompleteName, "");
        }
        return new MethodInfo(tipText, actualText);
    }

}
