package com.gitee.spirit.editor.core;

import com.gitee.spirit.core3.compile.entity.FileObject;
import com.gitee.spirit.core3.compile.entity.ParseTreeContext;
import com.gitee.spirit.core3.tree.api.ElementListenerAdapter;
import com.gitee.spirit.core3.tree.entity.ElementEvent;
import com.gitee.spirit.editor.entity.FileLine;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.net.URL;

@Data
@EqualsAndHashCode(callSuper = false)
public class CustomElementListener extends ElementListenerAdapter {

    private URL fileURL;
    private Integer lineNumber;
    private FileLineParser fileLineParser;
    private FileLine fileLine;
    private ElementEvent elementEvent;

    public CustomElementListener(URL fileURL, Integer lineNumber, FileLineParser fileLineParser) {
        this.fileURL = fileURL;
        this.lineNumber = lineNumber;
        this.fileLineParser = fileLineParser;
    }

    @Override
    public void beforeInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        if (isSameFileAndLineNumber(parseTreeContext, elementEvent)) {
            fileLine = fileLineParser.parseFileLine(elementEvent.getLine());
            elementEvent.setLine(fileLine.getLine());
        }
    }

    @Override
    public void afterInitialized(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        if (fileLine != null && this.elementEvent == null) {
            this.elementEvent = elementEvent;
        }
    }

    private boolean isSameFileAndLineNumber(ParseTreeContext parseTreeContext, ElementEvent elementEvent) {
        FileObject fileObject = parseTreeContext.getFileObject();
        return fileURL.sameFile(fileObject.getFileURL()) && lineNumber - 1 == elementEvent.getIndex();
    }

}
