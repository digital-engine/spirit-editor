package com.gitee.spirit.editor.core;

import cn.hutool.core.lang.Assert;
import com.gitee.spirit.core3.compile.utils.CharUtils;
import com.gitee.spirit.editor.entity.FileLine;
import org.springframework.stereotype.Component;

@Component
public class FileLineParser {

    public FileLine parseFileLine(String line) {
        Assert.isTrue(line.contains("@"), "No symbol '@' found!");
        FileLine fileLine = new FileLine();
        // 获取光标所在的定位
        int index = line.indexOf('@');
        // 截取光标前面的字符串
        line = line.substring(0, index + 1);
        for (int idx = index - 1; idx >= 0; idx--) {
            char ch = line.charAt(idx);
            if (ch == '(' || ch == '[' || ch == '{' || ch == '<') {
                boolean isTruncated = isTruncated(line, idx, ch);
                if (!isTruncated) {
                    line = line.substring(idx + 1);
                    break;
                }
            } else if (ch == ',' || ch == ' ') {
                line = line.substring(idx + 1);
                break;
            }
        }
        // 截取“.”后，不完整的成员名称
        String incompleteName = line.substring(line.lastIndexOf('.') + 1, line.lastIndexOf('@'));
        fileLine.setIncompleteName(incompleteName);
        // 截取“.”前，完整的变量名称
        line = line.substring(0, line.lastIndexOf('.'));
        // 将该行修改为返回语句
        if (!line.startsWith("return")) {
            line = "return " + line;
        }
        fileLine.setLine(line);
        return fileLine;
    }

    public boolean isTruncated(String line, int index, char leftChar) {
        Character rightChar = flipChar(leftChar);
        if (rightChar == null) return false;
        int end = CharUtils.findEndIndex(line, index, leftChar, rightChar);
        return end != -1;
    }

    private Character flipChar(char leftChar) {
        if (leftChar == '(') {
            return ')';

        } else if (leftChar == '[') {
            return ']';

        } else if (leftChar == '{') {
            return '}';

        } else if (leftChar == '<') {
            return '>';
        }
        return null;
    }

}
