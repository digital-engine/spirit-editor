package com.gitee.spirit.editor.entity;

import lombok.Data;

@Data
public class FileLine {
    private String incompleteName;
    private String line;
}
