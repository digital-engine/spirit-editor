package com.gitee.spirit.editor.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResObject<T> {

    private Integer code;
    private String message;
    private T data;

    public static <T> ResObject<T> successMsg(String message) {
        return new ResObject<>(200, message, null);
    }

    public static <T> ResObject<T> successData(T data) {
        return new ResObject<>(200, "success", data);
    }

    public static <T> ResObject<T> failMsg(String message) {
        return new ResObject<>(500, message, null);
    }

}
