package com.gitee.spirit.editor.service;

import com.gitee.spirit.editor.controller.EditorController;
import com.gitee.spirit.editor.entity.MethodInfo;
import com.gitee.spirit.editor.entity.ResObject;

import java.util.List;

public interface EditorService {

    ResObject<List<MethodInfo>> parseMethodInfos(EditorController.ParseMethodInfosDto dto);

}
