package com.gitee.spirit.editor.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.URLUtil;
import cn.hutool.setting.dialect.Props;
import com.gitee.spirit.common.constants.Config;
import com.gitee.spirit.common.utils.FileUtils;
import com.gitee.spirit.core3.compile.CoreCompiler;
import com.gitee.spirit.core3.visitor.api.CompilationLinker;
import com.gitee.spirit.core3.visitor.api.CompilationVisitor;
import com.gitee.spirit.core3.visitor.api.StatementParser;
import com.gitee.spirit.editor.entity.ResObject;
import com.gitee.spirit.editor.core.CustomElementListener;
import com.gitee.spirit.editor.core.CustomCompileListener;
import com.gitee.spirit.editor.controller.EditorController;
import com.gitee.spirit.editor.entity.MethodInfo;
import com.gitee.spirit.editor.service.EditorService;
import com.gitee.spirit.editor.core.FileLineParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.URL;
import java.util.*;

@Service
public class EditorServiceImpl implements EditorService {

    @Autowired
    private FileLineParser fileLineParser;
    @Autowired
    private CoreCompiler coreCompiler;
    @Autowired
    private StatementParser statementParser;
    @Autowired
    private CompilationLinker compilationLinker;
    @Autowired
    private CompilationVisitor compilationVisitor;

    public ResObject<List<MethodInfo>> parseMethodInfos(EditorController.ParseMethodInfosDto dto) {
        URL fileURL = URLUtil.getURL(new File(dto.getFilePath()));
        String filePath = fileURL.toString();
        filePath = filePath.substring(0, filePath.indexOf("/resources/sources"));
        filePath = filePath + "/resources/sources/compile_env.properties";
        if (!FileUtil.exist(filePath)) {
            return ResObject.failMsg("不存在compile_env.properties配置文件！");
        }

        Props props = new Props(filePath);
        String inputPath = props.getProperty("inputPath");
        String fileExtension = props.getProperty("fileExtension");

        File inputFile = new File(inputPath);
        Assert.isTrue(inputFile.isDirectory(), "The input path must be a directory!");
        URL inputFileURL = URLUtil.getURL(inputFile);
        String path = FileUtils.getPath(inputFileURL, fileURL, fileExtension);

        props.setProperty("targetPackage", path);
        props.setProperty("debugMode", false);

        CustomElementListener customElementListener = new CustomElementListener(fileURL, dto.getLineNumber(), fileLineParser);
        props.put(Config.ELEMENT_LISTENERS, customElementListener);
        CustomCompileListener customCompileListener = new CustomCompileListener(fileURL, dto.getContent(), customElementListener,
                statementParser, compilationLinker, compilationVisitor, null);
        props.put(Config.COMPILE_LISTENERS, customCompileListener);
        coreCompiler.compile(props);

        return customCompileListener.getResObject();
    }

}
