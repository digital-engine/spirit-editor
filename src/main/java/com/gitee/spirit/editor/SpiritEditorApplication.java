package com.gitee.spirit.editor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.gitee.spirit")
public class SpiritEditorApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpiritEditorApplication.class, args);
    }
}
